# Medical chain app

This app aims to make the medical treatment process easier among multiple stakeholders, including the patients, the doctors, and the pharmacies. By implementing IBM's Blockchain service, the patient's medical records are kept secure on a distributed network. The patient's medical history can be updated in real time using smart contracts. The data on the network is managed on a privilege basis; ie only the assigned doctor will be able to see a corresponding patient's medical history.
